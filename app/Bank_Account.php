<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank_Account extends Model
{
    public $timestamps = false;

    public function bank(){
    	return $this->belongsTo('App\Financial_Organization','financial_organization_id');
    }
}
