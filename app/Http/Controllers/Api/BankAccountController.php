<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Bank_Account;
use Carbon\Carbon;

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Bank_Account::with('bank')->whereNull('deleted_at')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'account_name' =>'required',
            'financial_organization_id' =>'required',
            'account_no' =>'required',
            'branch' =>'required',
            'account_type' =>'required',
            'swift_code' =>'required',
            'route_no' =>'required',
        ]);

        $Bank_Account = new Bank_Account();

        $Bank_Account->account_name = $request->account_name;
        $Bank_Account->financial_organization_id = $request->financial_organization_id;
        $Bank_Account->account_no = $request->account_no;
        $Bank_Account->branch = $request->branch;
        $Bank_Account->account_type = $request->account_type;
        $Bank_Account->swift_code = $request->swift_code;
        $Bank_Account->route_no = $request->route_no;
        $Bank_Account->created_at = Carbon::now();

        $Bank_Account->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'account_name' =>'required',
            'financial_organization_id' =>'required',
            'account_no' =>'required',
            'branch' =>'required',
            'account_type' =>'required',
            'swift_code' =>'required',
            'route_no' =>'required',
        ]);

        $Bank_Account = Bank_Account::findOrfail($id);

        $Bank_Account->account_name = $request->account_name;
        $Bank_Account->financial_organization_id = $request->financial_organization_id;
        $Bank_Account->account_no = $request->account_no;
        $Bank_Account->branch = $request->branch;
        $Bank_Account->account_type = $request->account_type;
        $Bank_Account->swift_code = $request->swift_code;
        $Bank_Account->route_no = $request->route_no;
        $Bank_Account->updated_at = Carbon::now();

        $Bank_Account->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Bank_Account = Bank_Account::findOrfail($id);
        $Bank_Account->deleted_at = Carbon::now();
        $Bank_Account->save();

    }
}
