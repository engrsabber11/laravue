<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Financial_Organization extends Model
{
    public $timestamps = false;

    public function bank_account(){
    	return $this->hasMany('App\Bank_Account');
    }
}
