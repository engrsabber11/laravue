<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('images/oxford.jpg')}}" alt="AdminLTE Logo" class="brand-image elevation-3">
      <span class="brand-text font-weight-light">OIS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://img.icons8.com/bubbles/2x/admin-settings-male.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <router-link to="/dashboard" class="nav-link">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
              </p>
            </router-link>
          </li>

          <!-- <li class="nav-item">
            <router-link to="/department" class="nav-link">
              <i class="nav-icon fa fa-flickr"></i>
              <p>
                Department
              </p>
            </router-link>
          </li>

          <li class="nav-item">
            <router-link to="/student" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Student
              </p>
            </router-link>
          </li> -->

          <li class="nav-item">
            <router-link to="/bankAccount" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Bank Account
              </p>
            </router-link>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
               <i class="nav-icon fa fa-power-off cyan"></i> {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
