/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'
import { Form, HasError, AlertError } from 'vform'

import Snotify, { SnotifyPosition } from 'vue-snotify'

const Snotifyoptions = {
  toast: {
    position: SnotifyPosition.rightTop
  }
}

Vue.use(Snotify, Snotifyoptions)

//vue Form
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

//vue Router
Vue.use(VueRouter)
let routes = [
  { path: '/dashboard', component: require('./components/DashboardComponent.vue').default},
  { path: '/bankAccount', component: require('./components/BankAccountComponent.vue').default},
]

const router = new VueRouter({
  routes,
  mode:'history'
});

//vue progressbar
import VueProgressBar from 'vue-progressbar'
const VueProgressBarOptions = {
    color: '#3490dc',
    failedColor: '#87111d',
    thickness: '5px',
    transition: {
        speed: '0.2s',
        opacity: '0.6s',
        termination: 300
    },
    autoRevert: true,
    location: 'top',
    inverse: false
};
Vue.use(VueProgressBar, VueProgressBarOptions);

Vue.component('pagination', require('./components/partial/PaginationComponent.vue').default);

const app = new Vue({
    el: '#app',
    router,
});
