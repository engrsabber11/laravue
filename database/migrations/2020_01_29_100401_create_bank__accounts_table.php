<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank__accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('financial_organization_id')->comment('Bank table primary id');
            $table->bigInteger('store_id')->nullable();
            $table->string('account_name');
            $table->char('account_no',100);
            $table->char('branch',50)->nullable();
            $table->tinyInteger('account_type')->nullable()->comment('1=Saving Account, 2=Current Account, 3=Join Account');
            $table->char('swift_code',100)->nullable();
            $table->char('route_no',100)->nullable();
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank__accounts');
    }
}
